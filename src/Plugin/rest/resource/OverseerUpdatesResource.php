<?php

namespace Drupal\overseer\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get the available project updates.
 *
 * @RestResource(
 *   id = "overseer_updates_resource",
 *   label = @Translation("Overseer updates resource"),
 *   uri_paths = {
 *     "canonical" = "/overseer/updates/{endpoint_path}"
 *   }
 * )
 */
class OverseerUpdatesResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->overseer = $container->get('overseer.controller');

    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($endpoint_path) {

    if ($this->overseer->isValidEndpointPath($endpoint_path)) {
      $update_information = $this->overseer->getAvailableUpdates(TRUE);
      $response = new ResourceResponse($update_information, 200);
    }
    else {
      // Wait some time before letting know that the endpoint is invalid.
      // This prevents brute force attacks on the /overseer/updates/{endpoint} path.
      sleep(rand(0.5, 3));
      $response = new ResourceResponse(['message' => $this->t('Invalid endpoint path.')], 403);
    }

    $cache = [
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return $response->addCacheableDependency($cache);
  }

  /**
   * Access is based on an access-token and is handled by the authenticated provider.
   *
   * @return array
   */
  public function permissions() {
    return [];
  }

}
