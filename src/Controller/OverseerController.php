<?php

namespace Drupal\overseer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OverseerController.
 */
class OverseerController extends ControllerBase {

  /**
   * Defines the Overseer cache name for the keyValue storage.
   */
  const OVERSEER_UPDATE_COLLECTION = 'overseer';

  /**
   * @var
   */
  protected $keyValue;

  /**
   * @var
   */
  protected $moduleHandler;

  /**
   * @var
   */
  protected $configFactory;

  /**
   * OverseerController constructor.
   */
  public function __construct() {
    $this->keyValue = \Drupal::keyValue(self::OVERSEER_UPDATE_COLLECTION);
    $this->moduleHandler = \Drupal::service('module_handler');
    $this->configFactory = \Drupal::configFactory();
  }

  /**
   * Gets the available updates for projects.
   *
   * @param bool $force_refresh
   * @return mixed
   */
  public function getAvailableUpdates($force_refresh = FALSE) {

    if ($force_refresh || $this->cacheNeedsRefresh()) {
      $this->refreshCache();
    }

    return $this->getFromCache();

  }

  /**
   * Returns TRUE if the given endpoint path is identical to the defined endpoint path.
   *
   * @param $endpoint_path
   * @return bool
   */
  public function isValidEndpointPath($endpoint_path) {

    return $endpoint_path === $this->getEndpointPath();

  }

  /**
   * Returns TRUE if the last fetch time is more than 600 seconds (10 minutes).
   *
   * @return bool
   */
  protected function cacheNeedsRefresh() {

    $last_fetch_time = $this->keyValue->get('updates.last_fetch_time');

    return ((time() - $last_fetch_time) > 600);

  }

  /**
   * Returns the update information from the keyValue table.
   *
   * @return mixed
   */
  protected function getFromCache() {

    return $this->keyValue->get('updates.update_information');

  }

  /**
   * Refreshes the cached date in the keyValue table.
   */
  protected function refreshCache() {

    // Get the actual update information.
    $update_information = $this->calculateAvailableUpdates();

    // Store the calculated update information.
    $this->keyValue->set('updates.update_information', $update_information);
    $this->keyValue->set('updates.last_fetch_time', time());

  }

  /**
   * Calculates the available updates for projects.
   *
   * @return mixed
   */
  protected function calculateAvailableUpdates() {

    // TODO: Remove this line when the following issue is fixed: https://www.drupal.org/project/drupal/issues/2920285
    // There is a vague issue with some updates. When the issue kicks in, some modules will never get update information.
    \Drupal::keyValue('update_fetch_task')->deleteAll();

    // Refresh the update data.
    \Drupal::service('update.manager')->refreshUpdateData();

    $available_updates = update_get_available(TRUE);
    $this->moduleHandler->loadInclude('update', 'compare.inc');
    $result = update_calculate_project_data($available_updates);

    return $result;

  }

  /**
   * Gets the settings from the settings form.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   */
  protected function getSettings() {

    return $this->configFactory->get('overseer.settings');

  }

  /**
   * Gets the endpoint path that is defined in the settings form.
   *
   * @return array|mixed|null
   */
  protected function getEndpointPath() {

    $settings = $this->getSettings();

    return $settings->get('endpoint_path');

  }

}
