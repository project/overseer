<?php

namespace Drupal\overseer\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AccessTokenAuthenticationProvider.
 */
class AccessTokenAuthenticationProvider implements AuthenticationProviderInterface {

  const ACCESS_TOKEN = 'X-ACCESS-TOKEN';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks whether suitable authentication credentials are on the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE if authentication credentials suitable for this provider are on the
   *   request, FALSE otherwise.
   */
  public function applies(Request $request) {
    if (substr($request->getPathInfo(), 0, 17) === '/overseer/updates') {
      // TODO: Read https://www.drupal.org/node/2352009
      // TODO: Read https://drupal.stackexchange.com/questions/222467/how-do-i-avoid-api-calls-are-cached
      \Drupal::service('page_cache_kill_switch')->trigger();

      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {

    $config = $this->configFactory->get('overseer.settings');
    $access_token = $request->headers->get(self::ACCESS_TOKEN) ?? $request->query->get(self::ACCESS_TOKEN);

    if (!$access_token) {
      // Immediately throw an access denied if there is no access token.
      throw new AccessDeniedHttpException();
      return null;
    }

    $saved_access_token = $config->get('access_token');

    if (is_null($saved_access_token) || $saved_access_token !== $access_token) {
      throw new AccessDeniedHttpException();
      return null;
    }

    $ip_restriction_enabled = $config->get('ip_restriction_enabled');

    if ($ip_restriction_enabled) {
      $client_ip = \Drupal::request()->getClientIp();
      $allowed_ips = explode("\r\n", $config->get('allowed_ip_addresses'));

      if (!in_array($client_ip, $allowed_ips)) {
        throw new AccessDeniedHttpException();
        return null;
      }
    }

    return $this->entityTypeManager->getStorage('user')->load(0);

  }

}
