<?php

namespace Drupal\overseer\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OverseerSettingsForm.
 */
class OverseerSettingsForm extends ConfigFormBase {

  /**
   * Defining the endpoint prefix as constant.
   */
  const OVERSEER_ENDPOINT_PREFIX = '/overseer/updates/';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'overseer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'overseer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('overseer.settings');
    $endpoint_path = $config->get('endpoint_path');

    if (!$endpoint_path || $endpoint_path === '') {
      $random = new Random();
      $endpoint_path = strtolower($random->name(32));
      \Drupal::messenger()->addWarning($this->t('Because no endpoint path was set, a random path segment is generated for you.<br><strong>Be aware:</strong> the random value is not saved yet.'));
    }
    else if (strlen($endpoint_path) < 12) {
      \Drupal::messenger()->addWarning($this->t('Your endpoint path seems rather short. Using an endpoint path of at least 12 characters is a lot safer.'));
    }

    $form['updates_route'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Updates'),
    ];

    $form['updates_route']['endpoint_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint path'),
      '#description' => $this->t('The API endpoint where the available updates information can be retrieved. You can come up with something yourself, but make sure the path is unique and not easy to guess.<br>You have to fill in this value (the part <strong>after</strong> ":overseer_endpoint_prefix") when creating a website in the Overseer app.', [':overseer_endpoint_prefix' => self::OVERSEER_ENDPOINT_PREFIX]),
      '#default_value' => $endpoint_path,
      '#required' => TRUE,
      '#attributes' => [
        'data-copy-button-id' => 'overseer-endpoint-path',
      ],
      '#field_prefix' => \Drupal::request()->getSchemeAndHttpHost() . self::OVERSEER_ENDPOINT_PREFIX,
      '#field_suffix' => new FormattableMarkup('<button onclick="event.preventDefault();var d=document;var ct=d.querySelector(\'[data-copy-button-id=overseer-endpoint-path]\');ct.select();ct.setSelectionRange(0, 99999);d.execCommand(\'copy\');this.textContent=\'Copied!\'">Copy</button>', []),
    ];

    $form['updates_route']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#description' => $this->t('The token that is needed to access the available updates API endpoint.'),
      '#default_value' => $config->get('access_token'),
    ];

    $form['updates_route']['ip_restriction_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable IP restriction'),
      '#default_value' => $config->get('ip_restriction_enabled'),
    ];

    $client_ip = \Drupal::request()->getClientIp();

    $form['updates_route']['allowed_ip_addresses'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed IP addresses'),
      '#description' => $this->t('Enter one IP address per line') . '<br />' . $this->t('Your current IP address is @ip_address', ['@ip_address' => $client_ip]),
      '#default_value' => $config->get('allowed_ip_addresses'),
      '#states' => [
        'visible' => [
          '[name="ip_restriction_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $endpoint_path = $form_state->getValue('endpoint_path');

    if (substr($endpoint_path, 0, 1) === '/') {
      $endpoint_path = trim($endpoint_path, '/');
      $form_state->setValue('endpoint_path', $endpoint_path);
    }

    if (!ctype_alnum($endpoint_path) || preg_match('/[A-Z]/', $endpoint_path)) {
      $form_state->setErrorByName('endpoint_path', $this->t('The endpoint path may only contain <strong>lowercase letters</strong> and <strong>digits</strong>. (a-z and 0-9)'));
    }

    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $existing_aliasses = \Drupal::entityTypeManager()->getStorage('path_alias')->loadByProperties([
      'alias' => self::OVERSEER_ENDPOINT_PREFIX . $endpoint_path,
      'langcode' => $language,
    ]);

    if (!empty($existing_aliasses)) {
      $form_state->setErrorByName('endpoint_path', $this->t('This path already exists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('overseer.settings')
      ->set('endpoint_path', $form_state->getValue('endpoint_path'))
      ->set('access_token', $form_state->getValue('access_token'))
      ->set('ip_restriction_enabled', $form_state->getValue('ip_restriction_enabled'))
      ->set('allowed_ip_addresses', $form_state->getValue('allowed_ip_addresses'))
      ->save();
  }

}
